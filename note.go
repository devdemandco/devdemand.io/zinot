package main

import (
	"time"

	"github.com/google/uuid"
)

type NoteType int

const (
	TypeText NoteType = iota
	TypeImage
	TypeEmbed
	TypeExtern
)

var nilTime = time.Time{}

type NoteHooks struct {
	OnUpdateContent func(oldNote *Note, content string, newID uuid.UUID, when time.Time)
	OnUpdateLabels  func(oldNote *Note, labels map[string]string, newID uuid.UUID, when time.Time)
	OnDelete        func(deletedNote *Note, when time.Time)
	OnCreate        func(newNote *Note, when time.Time)
}

type NoteMeta struct {
	Created time.Time
	Updated time.Time
	Deleted time.Time
	Labels  map[string]string
}

type NoteLink struct {
	Appendix int
	ID       uuid.UUID
	Static   bool // Static link is one that will always refer to the same version of note
}

type Note struct {
	ID          uuid.UUID
	Meta        NoteMeta
	Content     string
	Links       []*NoteLink
	PrevVersion *uuid.UUID
	Type        NoteType
	Hooks       *NoteHooks
}

func NewNote(content string, noteType NoteType, labels map[string]string, hooks *NoteHooks) (*Note, error) {
	now := time.Now()
	meta := NoteMeta{now, now, nilTime, labels}
	n := &Note{
		ID:          uuid.UUID{},
		Meta:        meta,
		Links:       nil,
		PrevVersion: nil,
		Type:        noteType,
		Hooks:       hooks,
	}

	err := n.parseContent()
	return n, err
}

func (n *Note) parseContent() error {
	// This function exists in case the notes need parsing
	// Mostly this exists just as a placeholder
	switch n.Type {
	case TypeExtern:
		return nil
	}
	return nil
}

// Copies an array of links
func copyLinks(nl []*NoteLink) []*NoteLink {
	if nl == nil {
		return nil
	}

	nnl := []*NoteLink{}
	for _, l := range nl {
		ln := new(NoteLink)
		*ln = *l
		nnl = append(nnl, ln)
	}

	return nnl
}

func copyHooks(h *NoteHooks) *NoteHooks {
	if h == nil {
		return nil
	}
	nh := &NoteHooks{}
	*nh = *h

	return nh
}

// Copies the value of a note from one place to another
func (n *Note) CopyNote() *Note {
	nn := new(Note)
	*nn = *n
	nn.Links = copyLinks(n.Links)
	nn.Hooks = copyHooks(n.Hooks)
	nn.ID = uuid.UUID{}
	return nn
}

// Updates the content of a note by creating
// a new note and making the new note reference the old
// copy
func (n *Note) UpdateContent(content string) (*Note, error) {
	t := time.Now()
	nn := n.CopyNote()
	nn.Content = content
	nn.Meta.Updated = t
	err := nn.parseContent()
	if n.Hooks != nil {
		n.Hooks.OnUpdateContent(n, content, nn.ID, t)
	}
	return nn, err
}

// Replaces the labels of a note with the ones provided
func (n *Note) UpdateLabels(labels map[string]string) *Note {
	nn := n.CopyNote()
	t := time.Now()
	nn.Meta.Updated = t
	nn.Meta.Labels = labels
	if n.Hooks != nil {
		n.Hooks.OnUpdateLabels(n, labels, nn.ID, t)
	}
	return nn
}

// Deletes a note by noting (ehehe) what time it was deleted
func (n *Note) DeleteNote() {
	t := time.Now()
	if n.Hooks != nil {
		n.Hooks.OnDelete(n, t)
	}
	n.Meta.Deleted = t
}

// Whether or not the note is deleted denoted by whether
// or not the Deleted time is zeroed
func (n *Note) IsDeleted() bool {
	return !n.Meta.Deleted.IsZero()
}
