package main

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"github.com/google/uuid"
)

type sqlStateKeeper struct {
	db *sql.DB
}

func NewSQLState(db *sql.DB) StateKeeper {
	return &sqlStateKeeper{db}
}

type SQLNote struct {
	*Note
	*sql.DB
}

func (n *SQLNote) RefreshLinks() error {
	rows, err := n.Query("SELECT * from notes_links WHERE note_id = ?", n.ID)
	if err != nil {
		return fmt.Errorf("error retrieving note_links: %s", n.ID)
	}
	defer rows.Close()

	var nls []*NoteLink
	for rows.Next() {
		var nl *NoteLink
		if err := rows.Scan(nl.ID, nl.Appendix); err != nil {
			return fmt.Errorf("failed retrieving note link: %s", n.ID)
		}
		nls = append(nls, nl)
	}

	if err := rows.Err(); err != nil {
		return fmt.Errorf("failed retrieving note links %s: %v", n.ID, err)
	}

	n.Links = nls
	return nil
}

func (n *SQLNote) RefreshLabels() error {
	rows, err := n.Query("SELECT key, value from notes_labels WHERE note_id = ?", n.ID)
	if err != nil {
		return fmt.Errorf("error retrieving note_links: %s", n.ID)
	}
	defer rows.Close()

	nls := map[string]string{}

	for rows.Next() {
		var key, value string
		if err := rows.Scan(key, value); err != nil {
			return fmt.Errorf("failed retrieving note link: %s", n.ID)
		}
		nls[key] = value
	}
	if err := rows.Err(); err != nil {
		return fmt.Errorf("failed retrieving note links %s: %v", n.ID, err)
	}

	n.Meta.Labels = nls
	return nil
}

func (s *sqlStateKeeper) GetNote(id uuid.UUID) (*Note, error) {
	var n *SQLNote
	n.DB = s.db
	row := s.db.QueryRow("SELECT * FROM notes WHERE id = ?", id)

	if err := row.Scan(n.ID, n.Content, n.PrevVersion, n.Meta.Created, n.Meta.Updated, n.Meta.Deleted, n.Type); err != nil {
		if err == sql.ErrNoRows {
			return n.Note, fmt.Errorf("could not find note: %s", id)
		}

		return n.Note, fmt.Errorf("error getting note %s: %v", id, err)
	}

	if err := n.RefreshLabels(); err != nil {
		return nil, err
	}

	if err := n.RefreshLinks(); err != nil {
		return nil, err
	}

	return n.Note, nil
}

// Updating a note is technically creating a new version,
// The only time we need to update an existing note would be if we were
// doing updates to links. Not sure what I want to do here so I'm just
// going to pretend that we only do updates for now.
func (s *sqlStateKeeper) Save(note *Note) error {
	if note.PrevVersion == nil {
		return fmt.Errorf("can't save a note that hasn't been created")
	}

	if note.PrevVersion != &note.ID {
		return fmt.Errorf("new note id, so cannot update")
	}

	ctx := context.Background()
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	// Remove old labels to cover any labels being deleted
	_, err = tx.ExecContext(ctx, `
		DELETE FROM notes_labels
		WHERE note_id = ?;
	`, note.ID)
	if err != nil {
		return err
	}

	err = s.createLabels(note, tx)
	if err != nil {
		return err
	}

	err = s.createLinks(note, tx)
	if err != nil {
		return err
	}

	// Update meta and content
	_, err = tx.Exec(`
		UPDATE notes
		SET content = ?,
		updated = ?,
		deleted = ?,
		created = ?,
		WHERE id = ?;
	`, note.Content, note.Meta.Updated, note.Meta.Deleted, note.Meta.Created, note.ID)
	if err != nil {
		return err
	}

	return tx.Commit()
}

func (s *sqlStateKeeper) Create(note *Note) error {
	if note.PrevVersion == &note.ID {
		return fmt.Errorf("existing note id, so needs update")
	}

	ctx := context.Background()
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	_, err = tx.Exec(`
		INSERT INTO notes
		(id, type, content, created, updated, deleted, prev_version)
		VALUES (?, ?, ?, ?, ?, ?)
	`, note.ID, note.Type, note.Content, note.Meta.Created, note.Meta.Updated, note.Meta.Deleted, note.PrevVersion)
	if err != nil {
		return err
	}

	err = s.createLabels(note, tx)
	if err != nil {
		return err
	}

	err = s.createLinks(note, tx)
	if err != nil {
		return err
	}

	return tx.Commit()
}

func (s *sqlStateKeeper) Delete(note *Note) error {
	nNote := note.CopyNote()
	nNote.Meta.Deleted = time.Now()

	return s.Save(note)
}

func (s *sqlStateKeeper) EnsureTables() error {
	query := `
	CREATE TABLE IF NOT EXISTS notes(
		id BLOB PRIMARY KEY NOT NULL,
		content TEXT NOT NULL,
		prev_version BLOB,
		created text NOT NULL,
		updated text NOT NULL,
		deleted text NOT NULL,
		type INT2 NOT NULL,
	);

	CREATE TABLE IF NOT EXISTS notes_links(
		note_id BLOB NOT NULL,
		appendix TEXT NOT NULL,
		static BOOL default false,
		PRIMARY KEY (note_id, appendix)
	)
	CREATE INDEX i_note_id ON notes_links (note_id, ASC);

	CREATE TABLE IF NOT EXISTS notes_labels(
		note_id BLOB NOT NULL,
		key TEXT NOT NULL,
		value TEXT NOT NULL,
	);
	CREATE INDEX i_meta_key ON notes_labels (key, ASC);
	`

	_, err := s.db.Exec(query)
	return err
}

func (s *sqlStateKeeper) createLabels(note *Note, tx *sql.Tx) error {
	lbStmt, err := tx.Prepare(`
	INSERT INTO notes_labels (
		note_id, key, value
	)
	VALUES (
		?, ?, ?
	);
`)
	if err != nil {
		return err
	}

	for key, value := range note.Meta.Labels {
		_, err := lbStmt.Exec(note.ID, key, value)
		if err != nil {
			return err
		}
	}

	return nil

}

func (s *sqlStateKeeper) createLinks(note *Note, tx *sql.Tx) error {
	linkStmt, err := tx.Prepare(`
		UPDATE notes_links
		SET note_id = ?
		SET static = ?
		WHERE appendix = ?;`)
	if err != nil {
		return err
	}

	for _, nLink := range note.Links {
		// NOTE: Potential bug here... static + link id update??
		linkStmt.Exec(nLink.ID, nLink.Static, nLink.Appendix)
	}

	return nil
}
