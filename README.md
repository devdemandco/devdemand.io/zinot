# Zinot
Zinot is a zettlekasten library that can be used within other projects
to add a graph-based knowledge memory structure with appropriate functionality.

## Project Status
v0 - Just working on a proof of concept, not sure that this will ever become anything,
but it may get used inside the other just-write-it project: go-hamx.