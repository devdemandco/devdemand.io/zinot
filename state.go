package main

import (
	"fmt"
	"slices"

	"github.com/google/uuid"
)

type StateKeeper interface {
	GetNote(id uuid.UUID) (*Note, error)
	Save(note *Note) error
	Create(note *Note) error
	Delete(note *Note) error
}

type stateKeeper struct {
	notes map[uuid.UUID]*Note
	links map[uuid.UUID][]uuid.UUID // Note key is linked to by values
}

func NewMemoryState(notes map[uuid.UUID]*Note) StateKeeper {
	n := map[uuid.UUID]*Note{}
	s := &stateKeeper{}
	if notes == nil {
		notes = n
	}

	s.notes = notes
	return s
}

func (s *stateKeeper) GetNote(id uuid.UUID) (*Note, error) {
	note, ok := s.notes[id]
	if !ok {
		return nil, fmt.Errorf("couldn't find note with the id %s", id)
	}

	return note, nil
}

func (s *stateKeeper) Create(note *Note) error {
	s.notes[note.ID] = note
	if _, ok := s.links[note.ID]; !ok {
		s.links[note.ID] = []uuid.UUID{}
	}

	// Updates the state-wide link store
	if note.Links != nil {
		for _, link := range note.Links {
			if links, ok := s.links[link.ID]; !ok {
				s.links[link.ID] = []uuid.UUID{note.ID}
			} else {
				if !slices.Contains(links, note.ID) {
					s.links[link.ID] = append(links, note.ID)
				}
			}
		}
	}

	return nil
}

func (s *stateKeeper) Save(note *Note) error {
	s.notes[note.ID] = note
	if _, ok := s.links[note.ID]; !ok {
		s.links[note.ID] = []uuid.UUID{}
	}

	var prevId *uuid.UUID
	if note.PrevVersion != nil {
		prevId = note.PrevVersion
	}

	// Updates the state-wide link store
	if note.Links != nil {
		for _, link := range note.Links {
			if links, ok := s.links[link.ID]; !ok {
				s.links[link.ID] = []uuid.UUID{note.ID}
			} else {
				s.links[link.ID] = append(links, note.ID)
			}
		}
	}

	err := s.updateReferences(*prevId, note.ID)
	return err
}

// Goes through notes and updates old references to new ones if not static refs
func (s *stateKeeper) updateReferences(prevId, newId uuid.UUID) error {
	notesToUpdate := s.links[prevId]
	if notesToUpdate == nil {
		return nil
	}

	for _, noteId := range notesToUpdate {
		note := s.notes[noteId]
		if note == nil {
			return fmt.Errorf("failed retrieving note to update references: %s", noteId)
		}

		for _, link := range note.Links {
			if link.ID == prevId && !link.Static {
				link.ID = newId
			}
		}
	}

	return nil
}

func (s *stateKeeper) Delete(note *Note) error {
	if _, ok := s.notes[note.ID]; ok {
		delete(s.notes, note.ID)
		return nil
	}

	return fmt.Errorf("note didn't exist: %s", note.ID)
}
